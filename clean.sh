#!/bin/bash
find . -type f -name '*~' | xargs rm -f
rm -rf dist
rm -rf astblick.egg-info
